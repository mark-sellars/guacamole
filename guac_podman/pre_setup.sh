#!/bin/sh
#
# Make Required folders
echo "Makeing required directories"
mkdir ./{data,drive,record} >/dev/null 2>&1
echo "done"

# Set owner and permissions
echo "Setting directory owner, group, and permissions"
chown -R root:root data drive guacamole_home init nginx record
chmod -R 775 data drive guacamole_home init nginx record   

# Setup for postgres
echo "Preparing folder init and creating ./init/initdb.sql"
mkdir ./init >/dev/null 2>&1
chmod -R +x -- ./init
podman run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --postgres > ./init/initdb.sql
echo "done"
